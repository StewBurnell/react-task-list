const _ = require('lodash');
const React = require('react');

/**
 * The TaskList component renders a view for a list
 * of tasks.
 */
const TaskList = React.createClass({
  // Display name for the component (useful for debugging)
  displayName: 'TaskList',
  getInitialState: function() {
    return {name: 'My real task list'};
  },

  // Describe how to render the component
  render: function() {
    const d = this.props.showMessage;
    const taskList = _.map(this.props.myTasks, task =>
    <Task key={task.id} {...task} />);

    const onNameChange = (event) => {
    this.setState({name: event.target.value});
    }

    const onButtonClick = (event) => {
    this.props.showMessage("Hello, " + this.state.name + "!");
    }

    return (
      <div>
        <input type="text" value={this.state.name} onChange={onNameChange}></input>
        <button onClick={onButtonClick}>Click me!</button>
        <ul>
          {taskList}
        </ul>
      </div>
    );
  }
});



// Export the TaskList component
module.exports = TaskList;
//module.exports = Task;
const Task = React.createClass({
  dislayName: "Task",
  getDefaultProps: function() {
    return {
      description:'TBA'
    };
  },
  render: function() {
    return(

    <li>{this.props.id + ', ' + this.props.description + ', ' + this.props.completed}
      <input type="checkbox" checked={this.props.completed} readOnly />
    </li>
    );
  }
});